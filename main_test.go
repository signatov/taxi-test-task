package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
	"time"
)

func checkBidID(t *testing.T, id string) {
	if len(id) != idLen {
		t.Error("bid id has inappropriate length")
	}
	for i := range id {
		if id[i] < 'a' || id[i] > 'z' {
			t.Error("bid id contains no alphabet symbols")
		}
	}
}

func TestGenerateBidID(t *testing.T) {
	checkBidID(t, generateBidID())
}

func TestNewTaxiService(t *testing.T) {
	srv := NewTaxiService()
	if srv.mutex == nil {
		t.Error("service should have a inited mutex")
	}
	if len(srv.queue) != initialBidsCount {
		t.Errorf("queue should have %d bids", initialBidsCount)
	}
	if len(srv.usedIDs) != initialBidsCount {
		t.Error("used map should not be empty")
	}
	for k, v := range srv.usedIDs {
		checkBidID(t, k)
		if v < 0 || v >= initialBidsCount {
			t.Error("mismatch position index")
		}
	}
}

func TestNewBidUnsync(t *testing.T) {
	srv := &TaxiService{
		queue:   make([]Bid, 0, 2),
		usedIDs: make(map[string]int),
		mutex:   &sync.RWMutex{},
	}
	srv.newBidUnsync()
	if len(srv.queue) != 1 {
		t.Error("should be one element in queue")
	}
	if len(srv.usedIDs) != 1 {
		t.Error("should be one element in map")
	}
	k := srv.queue[0].ID
	v, ok := srv.usedIDs[k]
	if !ok {
		t.Error("map and queue should have the same bid ID")
	}
	if v != 0 {
		t.Error("index of one element should be 0")
	}
}

func generateFullCancelledQueue(srv *TaxiService) {
	id := make([]byte, idLen)
	for i := range alphabet {
		for j := range alphabet {
			id[0] = alphabet[i]
			id[1] = alphabet[j]
			pos := len(srv.queue)
			bid := Bid{ID: string(id), Cancelled: true}
			srv.queue = append(srv.queue, bid)
			srv.usedIDs[bid.ID] = pos
		}
	}
}

func checkUpdateQueue(t *testing.T, srv *TaxiService) {
	updated := 0
	for i := range srv.queue {
		if !srv.queue[i].Cancelled {
			updated++
		}
	}
	if updated != 1 {
		t.Error("should be updated only one element")
	}
}

func TestNewBidUnsyncCancelled(t *testing.T) {
	srv := &TaxiService{
		queue:   make([]Bid, 0, len(alphabet)*len(alphabet)),
		usedIDs: make(map[string]int),
		mutex:   &sync.RWMutex{},
	}
	generateFullCancelledQueue(srv)
	srv.newBidUnsync()
	checkUpdateQueue(t, srv)
}

func TestNewBid(t *testing.T) {
	const sleepTime = 200 * time.Millisecond
	srv := &TaxiService{
		queue:   make([]Bid, 0, 2),
		usedIDs: make(map[string]int),
		mutex:   &sync.RWMutex{},
	}
	srv.mutex.Lock()
	w := sync.WaitGroup{}
	w.Add(1)
	var elapsed time.Duration
	go func() {
		start := time.Now()
		srv.newBid()
		elapsed = time.Since(start)
		w.Done()
	}()
	time.Sleep(sleepTime)
	srv.mutex.Unlock()
	w.Wait()
	if elapsed < sleepTime {
		t.Errorf("code execution time should be no less %v", sleepTime)
	}
	if len(srv.queue) != 1 {
		t.Error("should be one element in queue")
	}
	if len(srv.usedIDs) != 1 {
		t.Error("should be one element in map")
	}
	k := srv.queue[0].ID
	v, ok := srv.usedIDs[k]
	if !ok {
		t.Error("map and queue should have the same bid ID")
	}
	if v != 0 {
		t.Error("index of one element should be 0")
	}
}

func TestNewBidCancelled(t *testing.T) {
	srv := &TaxiService{
		queue:   make([]Bid, 0, len(alphabet)*len(alphabet)),
		usedIDs: make(map[string]int),
		mutex:   &sync.RWMutex{},
	}
	generateFullCancelledQueue(srv)
	srv.newBid()
	checkUpdateQueue(t, srv)
}

func TestCancelBid(t *testing.T) {
	srv := NewTaxiService()
	srv.cancelBid()
	cancelled := 0
	for _, v := range srv.queue {
		if v.Cancelled {
			cancelled++
		}
	}
	if cancelled != 1 {
		t.Error("should be only one cancelled bid")
	}
}

func (ctx *appContext) handleBidHandler(w http.ResponseWriter, r *http.Request) {
	code, err := ctx.handleBid(w, r)
	if err != nil {
		switch code {
		case http.StatusNotFound:
			http.NotFound(w, r)
		case http.StatusInternalServerError:
			http.Error(w, http.StatusText(code), code)
		default:
			http.Error(w, http.StatusText(code), code)
		}
	}
}

func (ctx *appContext) handleBidListHandler(w http.ResponseWriter, r *http.Request) {
	code, err := ctx.handleBidList(w, r)
	if err != nil {
		switch code {
		case http.StatusNotFound:
			http.NotFound(w, r)
		case http.StatusInternalServerError:
			http.Error(w, http.StatusText(code), code)
		default:
			http.Error(w, http.StatusText(code), code)
		}
	}
}

func TestHandleBid(t *testing.T) {
	r, err := http.NewRequest("GET", "/request", nil)
	if err != nil {
		t.Fatal(err)
	}

	w := httptest.NewRecorder()
	ctx := &appContext{srv: NewTaxiService()}
	handler := http.HandlerFunc(ctx.handleBidHandler)
	handler.ServeHTTP(w, r)
	if status := w.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
	if w.HeaderMap.Get("Content-Type") != "application/json" {
		t.Error("response should be application/json")
	}
	var bid string
	err = json.Unmarshal(w.Body.Bytes(), &bid)
	if err != nil {
		t.Errorf("malformed response body: %v", err)
	}
	checkBidID(t, bid)
}

func TestHandleBidList(t *testing.T) {
	r, err := http.NewRequest("GET", "/admin/requests", nil)
	if err != nil {
		t.Fatal(err)
	}

	w := httptest.NewRecorder()
	ctx := &appContext{srv: NewTaxiService()}
	handler := http.HandlerFunc(ctx.handleBidListHandler)
	handler.ServeHTTP(w, r)
	if status := w.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
	if w.HeaderMap.Get("Content-Type") != "application/json" {
		t.Error("response should be application/json")
	}
	var bids []Bid
	err = json.Unmarshal(w.Body.Bytes(), &bids)
	if err != nil {
		t.Errorf("malformed response body: %v", err)
	}
	if len(bids) != 0 {
		t.Error("should be empty queue")
	}
}

func TestHandleBidListWithReallyBids(t *testing.T) {
	r, err := http.NewRequest("GET", "/admin/requests", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr, err := http.NewRequest("GET", "/request", nil)
	if err != nil {
		t.Fatal(err)
	}

	w := httptest.NewRecorder()
	ctx := &appContext{srv: NewTaxiService()}
	newBidHandler := http.HandlerFunc(ctx.handleBidHandler)
	newBidHandler.ServeHTTP(w, rr)
	newBidHandler.ServeHTTP(w, rr)
	newBidHandler.ServeHTTP(w, rr)
	newBidHandler.ServeHTTP(w, rr)
	w = httptest.NewRecorder()
	handler := http.HandlerFunc(ctx.handleBidListHandler)
	handler.ServeHTTP(w, r)
	if status := w.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
	if w.HeaderMap.Get("Content-Type") != "application/json" {
		t.Error("response should be application/json")
	}
	var bids []Bid
	err = json.Unmarshal(w.Body.Bytes(), &bids)
	if err != nil {
		t.Errorf("malformed response body: %v: %v", err, string(w.Body.Bytes()))
	}
	if len(bids) != 4 {
		t.Error("should be empty queue")
	}
}
