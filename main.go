package main

import (
	"context"
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/pkg/errors"
)

const (
	initialBidsCount = 50
	idLen            = 2
	updateInterval   = 200 * time.Millisecond
	bindAddress      = ":8080"
	alphabet         = "abcdefghijklmnopqrstuvwxyz"
)

// Bid is a taxi driver application
type Bid struct {
	ID        string
	Cancelled bool `json:"-"`
	Shown     int
}

// TaxiService is a taxi park application-processing service
type TaxiService struct {
	queue   []Bid
	usedIDs map[string]int
	mutex   *sync.RWMutex
}

func generateBidID() string {
	id := make([]byte, idLen)
	for i := range id {
		id[i] = alphabet[rand.Intn(len(alphabet))]
	}
	return string(id)
}

// NewTaxiService is a function that makes new taxi service instance.
func NewTaxiService() *TaxiService {
	srv := &TaxiService{
		queue:   make([]Bid, 0, len(alphabet)*len(alphabet)),
		usedIDs: make(map[string]int),
		mutex:   &sync.RWMutex{},
	}
	for i := 0; i < initialBidsCount; i++ {
		srv.newBidUnsync()
	}
	return srv
}

func (srv *TaxiService) newBidUnsync() Bid {
	bid := Bid{}
	for {
		id := generateBidID()
		updated := false
		pos, ok := srv.usedIDs[id]
		if !ok {
			bid.ID = id
			srv.usedIDs[id] = len(srv.queue)
			srv.queue = append(srv.queue, bid)
			updated = true
		} else if srv.queue[pos].Cancelled {
			srv.queue[pos].Cancelled = false
			bid = srv.queue[pos]
			updated = true
		}
		if updated {
			break
		}
	}
	return bid
}

func (srv *TaxiService) newBid() Bid {
	bid := Bid{}
	for {
		id := generateBidID()
		updated := false
		srv.mutex.Lock()
		pos, ok := srv.usedIDs[id]
		if !ok {
			bid.ID = id
			srv.usedIDs[id] = len(srv.queue)
			srv.queue = append(srv.queue, bid)
			updated = true
		} else if srv.queue[pos].Cancelled {
			srv.queue[pos].Cancelled = false
			bid = srv.queue[pos]
			updated = true
		}
		srv.mutex.Unlock()
		if updated {
			break
		}
	}
	return bid
}

func (srv *TaxiService) cancelBid() {
	for {
		updated := false
		srv.mutex.Lock()
		pos := rand.Intn(len(srv.queue))
		if !srv.queue[pos].Cancelled {
			srv.queue[pos].Cancelled = true
			updated = true
		}
		srv.mutex.Unlock()
		if updated {
			break
		}
	}
}

type appContext struct {
	srv *TaxiService
}

func (ctx *appContext) handleBidList(w http.ResponseWriter, r *http.Request) (int, error) {
	ctx.srv.mutex.RLock()
	shown := make([]Bid, 0, len(ctx.srv.queue))
	for _, v := range ctx.srv.queue {
		if v.Shown == 0 {
			continue
		}
		shown = append(shown, v)
	}
	ctx.srv.mutex.RUnlock()

	buf, err := json.Marshal(shown)
	if err == nil {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		w.Write(buf)
	} else {
		http.Error(w, "Can't marshal structure to JSON message", http.StatusInternalServerError)
		return 500, errors.Wrap(err, "Can't marshal structure to JSON message")
	}
	return 200, nil
}

func (ctx *appContext) handleBid(w http.ResponseWriter, r *http.Request) (int, error) {
	for {
		ctx.srv.mutex.RLock()
		pos := rand.Intn(len(ctx.srv.queue))
		cancelled := ctx.srv.queue[pos].Cancelled
		ctx.srv.mutex.RUnlock()
		if !cancelled {
			ctx.srv.mutex.Lock()
			ctx.srv.queue[pos].Shown++
			bid := ctx.srv.queue[pos].ID
			ctx.srv.mutex.Unlock()
			buf, err := json.Marshal(bid)
			if err == nil {
				w.WriteHeader(http.StatusOK)
				w.Header().Set("Content-Type", "application/json")
				w.Write(buf)
			} else {
				http.Error(w, "Can't marshal structure to JSON message", http.StatusInternalServerError)
				return 500, errors.Wrap(err, "Can't marshal structure to JSON message")
			}
			break
		}
	}
	return 200, nil
}

type safeHandler func(w http.ResponseWriter, r *http.Request) (int, error)

// ServerHttp is a HTTP-request processing handler.
func (fn safeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	status, err := fn(w, r)
	if err != nil {
		log.Printf("HTTP %d: %q", status, err)
		switch status {
		case http.StatusNotFound:
			http.NotFound(w, r)
		case http.StatusInternalServerError:
			http.Error(w, http.StatusText(status), status)
		default:
			http.Error(w, http.StatusText(status), status)
		}
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())
	ctx := &appContext{srv: NewTaxiService()}

	ticker := time.NewTicker(updateInterval)
	go func() {
		for range ticker.C {
			ctx.srv.cancelBid()
			ctx.srv.newBid()
		}
	}()

	mux := http.NewServeMux()
	mux.Handle("/admin/requests", safeHandler(ctx.handleBidList))
	mux.Handle("/request", safeHandler(ctx.handleBid))

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	server := &http.Server{Addr: bindAddress, Handler: mux}
	go func() {
		log.Fatal(server.ListenAndServe())
	}()
	log.Printf("The service is ready to listen %s and serve.", bindAddress)

	sig := <-interrupt
	switch sig {
	case os.Interrupt:
		log.Print("SIGINT...")
	case syscall.SIGTERM:
		log.Print("SIGTERM...")
	}

	ticker.Stop()
	server.Shutdown(context.Background())
}
